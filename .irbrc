#!/usr/bin/env ruby
require 'irb/completion'
require 'irb/ext/save-history'

IRB.conf[:AUTO_INDENT] = true
IRB.conf[:PROMPT_MODE] = :SIMPLE
IRB.conf[:SAVE_HISTORY] = 1000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb_history"
RB_START_TIME = Time.now

# Prompt behavior
ARGV.concat [ "--readline", "--prompt-mode", "simple" ]

# ActiveRecord::Base.logger.level = 1 if defined? ActiveRecord::Base

def y(obj)
  puts obj.to_yaml
end

def clear
  system('clear')
end
alias :cl :clear

alias :q :exit

class Object
  def vim(method_name)
    file, line = method(method_name).source_location
    `vim '#{file}' +#{line}`
  end

  def mate(method_name)
   file, line = method(method_name).source_location
   `mate '#{file}' -l #{line}`
  end

  def local_methods
    (methods - Object.instance_methods).sort
  end
end

class Class
  def class_methods
    (methods - Class.instance_methods - Object.methods).sort
  end
end

# Break out of the Bundler jail
# from https://github.com/ConradIrwin/pry-debundle/blob/master/lib/pry-debundle.rb
if defined? Bundler
  Gem.post_reset_hooks.reject! { |hook| hook.source_location.first =~ %r{/bundler/} }
  Gem::Specification.reset
  load 'rubygems/custom_require.rb'
end

if defined? Rails
  begin
    require 'hirb'
    Hirb.enable
  rescue LoadError
  end

  #Redirect log to STDOUT, which means the console itself
  IRB.conf[:IRB_RC] = Proc.new do
    logger = Logger.new(STDOUT)
    ActiveRecord::Base.logger = logger
    ActiveResource::Base.logger = logger
    ActiveRecord::Base.instance_eval { alias :[] :find }
  end

  ### RAILS SPECIFIC HELPER METHODS
  # TODO: DRY this out
  def log_ar_to(stream)
    ActiveRecord::Base.logger = expand_logger stream
    reload!
  end

  def log_ac_to(stream)
    logger = expand_logger stream
    ActionController::Base.logger = expand_logger stream
    reload!
  end

  def expand_logger(name)
    logger = name
    Logger.new logger
  end
end
