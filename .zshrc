# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="clean"

# Example aliases
alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias st='open -a "Sublime Text 2"'
alias linode_sav="ssh savitsky@178.79.169.211"
alias linode_root="ssh root@178.79.169.211"
alias linode_deployer="ssh deployer@178.79.169.211"
alias gdf="git diff --cached"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
plugins=(git osx ruby rails3 brew bundler rbenv nmap)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=/opt/local/bin:/opt/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin

# ImageMagick
# export MAGICK_HOME="/opt"
# export PATH="$MAGICK_HOME/bin:$PATH"
# export DYLD_LIBRARY_PATH="$MAGICK_HOME/lib"

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# Rbenv
export PATH="/usr/local/sbin:/usr/local/bin:$PATH"
export RBENV_ROOT=/usr/local/opt/rbenv
eval "$(rbenv init -)"

# Less
export LESS='-i-P%f (%i/%m) Line %lt/%L'

alias uni_restart="cap unicorn:stop && cap unicorn:start"
